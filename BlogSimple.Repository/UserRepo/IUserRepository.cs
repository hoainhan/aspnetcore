﻿using BlogSimple.Model.Domain;

namespace BlogSimple.Repository.UserRepo
{
    public interface IUserRepository: IGenericRepository<User>
    {
    }
}
