﻿using BlogSimple.Model;
using BlogSimple.Model.Domain;
using System.Linq;

namespace BlogSimple.Repository.UserRepo
{
    public class UserRepository: GenericRepository<User, BlogSimpleContext>, IUserRepository
    {
        private BlogSimpleContext _blogSimpleContext;
        public UserRepository(BlogSimpleContext dbContext) : base(dbContext)
        {
            _blogSimpleContext = dbContext;
        }
        public bool CheckExistUser()
        {
            var users = _blogSimpleContext.Users.AsQueryable();
            var blogPosts = _blogSimpleContext.BlogPosts.AsQueryable();
            var result = from u in users
                         join bp in blogPosts on u.Id equals bp.CreatedBy
                         select new { UserName = u.UserName };

            var result1 = from u in users
                          group u by u.Id into group1
                          select new { Id = group1.Key, Name = group1.Select(x => x.LastName) };
            return result.Count() >0;
        }
    }
}
