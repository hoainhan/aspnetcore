﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogSimple.Model.Domain;
using BlogSimple.Repository;

namespace BlogSimple.Repository
{
    public interface ICategoryRepository: IGenericRepository<Category>
    {
        Category GetById(Guid id);
    }
}
