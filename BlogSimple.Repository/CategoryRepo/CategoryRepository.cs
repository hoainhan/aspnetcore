﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BlogSimple.Model;
using BlogSimple.Model.Domain;
using BlogSimple.Repository;

namespace BlogSimple.Repository
{
    public class CategoryRepository : GenericRepository<Category,BlogSimpleContext>, ICategoryRepository
    {
        public CategoryRepository(BlogSimpleContext dbContext) : base(dbContext)
        {
        }
    }
}
