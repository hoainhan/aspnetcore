﻿using BlogSimple.Model.Domain;

namespace BlogSimple.Repository.Localization
{
    public interface ICultureRepository: IGenericRepository<Culture>
    {
    }
}
