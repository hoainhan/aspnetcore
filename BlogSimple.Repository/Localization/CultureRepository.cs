﻿using BlogSimple.Model;
using BlogSimple.Model.Domain;
using System.Linq;

namespace BlogSimple.Repository.Localization
{
    public class CultureRepository: GenericRepository<Culture, BlogSimpleContext>, ICultureRepository
    {
        private BlogSimpleContext _blogSimpleContext;
        public CultureRepository(BlogSimpleContext dbContext) : base(dbContext)
        {
            _blogSimpleContext = dbContext;
        }
        
    }
}
