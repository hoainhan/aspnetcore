﻿using BlogSimple.Model;
using BlogSimple.Model.Domain;
using System.Linq;

namespace BlogSimple.Repository.Localization
{
    public class ResourceRepository : GenericRepository<Resource, BlogSimpleContext>, IResourceRepository
    {
        private BlogSimpleContext _blogSimpleContext;
        public ResourceRepository(BlogSimpleContext dbContext) : base(dbContext)
        {
            _blogSimpleContext = dbContext;
        }
        
    }
}
