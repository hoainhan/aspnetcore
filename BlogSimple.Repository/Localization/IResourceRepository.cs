﻿using BlogSimple.Model.Domain;

namespace BlogSimple.Repository.Localization
{
    public interface IResourceRepository : IGenericRepository<Resource>
    {
    }
}
