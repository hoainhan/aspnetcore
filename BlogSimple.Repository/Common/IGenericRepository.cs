﻿using BlogSimple.Model.Common;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogSimple.Repository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        Task<List<T>> GetAllAsync();
        void Insert(T entity);
        Task<EntityEntry<T>> InsertAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(Guid Id);
        IEnumerable<T> where(Expression<Func<T,bool>> predicate);
    }
}
