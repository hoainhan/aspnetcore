﻿using BlogSimple.Model.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace BlogSimple.Repository
{
    public abstract class GenericRepository<T, TContext> : IGenericRepository<T> where T : BaseEntity where TContext:DbContext
    {
        private readonly TContext _dbContext;
        private readonly DbSet<T> _entity;

    protected GenericRepository(TContext dbContext)
    {
        _dbContext = dbContext;
        _entity = dbContext.Set<T>();
    }

    public void Delete(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException();
        _dbContext.Remove(entity);
    }

    public IEnumerable<T> GetAll()
    {
        return _entity.AsEnumerable();
    }
    
    public async Task<List<T>> GetAllAsync()
    {
        return await _entity.ToListAsync();
    }
    public void Insert(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException();

        _dbContext.Add(entity);
    }
    public async Task<EntityEntry<T>> InsertAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException();

       return await _dbContext.AddAsync(entity);
    }
    public void Update(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException();
            _dbContext.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
    }
    public virtual T GetById (Guid Id)
    {
          return _entity.Find(Id);
    }
    public IEnumerable<T> where(Expression<Func<T, bool>> predicate)
    {
        return _entity.Where(predicate).AsEnumerable();
    }

    }
}
