﻿using BlogSimple.Model;
using BlogSimple.Model.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BlogSimple.Repository
{
    public class BlogPostRepository : GenericRepository<BlogPost,BlogSimpleContext>, IBlogPostRepository
    {
        private BlogSimpleContext _blogSimpleContext;
        public BlogPostRepository(BlogSimpleContext dbContext) : base(dbContext)
        {
            _blogSimpleContext = dbContext;
        }
        public override BlogPost GetById(Guid id)
        {
            return _blogSimpleContext.BlogPosts.Where(x => x.Id == id).Include(x => x.Comments).FirstOrDefault();
        }
    }
}
