﻿using BlogSimple.Model.Domain;
using System;

namespace BlogSimple.Repository
{
    public interface IBlogPostRepository: IGenericRepository<BlogPost>
    {
        
    }
}
