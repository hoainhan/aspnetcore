﻿using BlogSimple.Model.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogSimple.Model.Domain
{
    public class BlogPost:Auditable<Guid>
    {
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public byte[] ThumbnailImage { get; set; }
        [ForeignKey("Category")]
        public Guid CategoryId { get; set; }
        [NotMapped]
        public IFormFile Image { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public Category Category { get; set; }
    }
}
