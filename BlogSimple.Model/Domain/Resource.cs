﻿using BlogSimple.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BlogSimple.Model.Domain
{
    public class Resource: Entity<int>
    {
        public string Key { get; set; }

        public string Value { get; set; }
        [ForeignKey("CultureId")]
        public string CultureId { get; set; }

        public Culture Culture { get; set; }
    }
}
