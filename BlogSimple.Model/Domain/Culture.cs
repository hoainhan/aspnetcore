﻿using BlogSimple.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSimple.Model.Domain
{
    public class Culture: Entity<string>
    {
        public Culture(string id)
        {
            Id = id;
        }
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public IList<Resource> Resources { get; set; }
    }

}
