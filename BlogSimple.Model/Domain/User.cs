﻿using BlogSimple.Model.Common;
using System;

namespace BlogSimple.Model.Domain
{
    public class User:Auditable<Guid>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
    }
}
