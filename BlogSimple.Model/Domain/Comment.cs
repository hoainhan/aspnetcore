﻿using BlogSimple.Model.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogSimple.Model.Domain
{
    public class Comment:Auditable<Guid>
    {
        public string CommentDescription { get; set; }
        [ForeignKey("BlogPost")]
        public Guid BlogPostId { get; set; }
        public BlogPost BlogPost { get; set; }
    }
}
