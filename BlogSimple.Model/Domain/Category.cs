﻿using BlogSimple.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSimple.Model.Domain
{
    public class Category:Auditable<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<BlogPost> BlogPosts { get; set; }
    }
}
