﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlogSimple.Model.Migrations
{
    public partial class inittableanddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Culture",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Culture", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 200, nullable: false),
                    LastName = table.Column<string>(maxLength: 200, nullable: false),
                    Password = table.Column<string>(nullable: true),
                    PasswordSalt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlogPosts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    ShortDescription = table.Column<string>(nullable: true),
                    Content = table.Column<string>(maxLength: 5000, nullable: false),
                    ThumbnailImage = table.Column<byte[]>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlogPosts_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Resource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    CultureId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resource", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resource_Culture_CultureId",
                        column: x => x.CultureId,
                        principalTable: "Culture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<Guid>(maxLength: 256, nullable: true),
                    CommentDescription = table.Column<string>(maxLength: 5000, nullable: false),
                    BlogPostId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_BlogPosts_BlogPostId",
                        column: x => x.BlogPostId,
                        principalTable: "BlogPosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Culture",
                columns: new[] { "Id", "IsDefault", "Name" },
                values: new object[] { "en-US", true, "English (US)" });

            migrationBuilder.InsertData(
                table: "Culture",
                columns: new[] { "Id", "IsDefault", "Name" },
                values: new object[] { "vi-VN", false, "Tiếng Việt (VN)" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 1, "vi-VN", "Blog Simple", "Blog Đơn Giản" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 2, "vi-VN", "Logout", "Thoát" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 3, "vi-VN", "Login", "Đăng Nhập" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 4, "vi-VN", "Search", "Tìm Kiếm" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 5, "vi-VN", "Search", "Đi" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 6, "vi-VN", "Go", "Tìm Kiếm" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 7, "vi-VN", "Categories", "Loại" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 8, "vi-VN", "Hello", "Chào" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 9, "vi-VN", "Your Comment", "Bình Luận Của Bạn" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 10, "vi-VN", "Total Comment(s)", "Tổng Bình Luận" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 11, "vi-VN", "Author", "Tác Giả" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 12, "vi-VN", "Email is not exist", "Email thì không tồn tại" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 13, "vi-VN", "The email is invalid", "Email thì không hợp lệ" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 14, "vi-VN", "Create User", "Tạo Người Dùng" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "CultureId", "Key", "Value" },
                values: new object[] { 15, "vi-VN", "The Email field is required.", "Email thì bắt buộc" });

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_CategoryId",
                table: "BlogPosts",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BlogPostId",
                table: "Comments",
                column: "BlogPostId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_CultureId",
                table: "Resource",
                column: "CultureId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Resource");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "BlogPosts");

            migrationBuilder.DropTable(
                name: "Culture");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
