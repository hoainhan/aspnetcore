﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogSimple.Model.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogSimple.Model.ConfigMap
{
    public class ConfigMaps
    {
        public static void MapBlogPost(EntityTypeBuilder<BlogPost> entityBuilder)
        {
            entityBuilder.HasKey(x => x.Id);
           
            entityBuilder.Property(x => x.Content).HasMaxLength(5000).IsRequired();
            entityBuilder.HasMany(x => x.Comments).WithOne(x => x.BlogPost);
        }
        public static void MapComment(EntityTypeBuilder<Comment> entityBuilder)
        {
            entityBuilder.HasKey(x => x.Id);

            entityBuilder.Property(x => x.CommentDescription).HasMaxLength(5000).IsRequired();
            entityBuilder.HasOne(x => x.BlogPost).WithMany(x => x.Comments).IsRequired();
        }
        public static void MapCategory(EntityTypeBuilder<Category> entityBuilder)
        {
            entityBuilder.HasKey(x=>x.Id);
            entityBuilder.Property(x => x.Name).IsRequired().HasMaxLength(200);
            entityBuilder.HasMany(x => x.BlogPosts).WithOne(x => x.Category);
        }
        public static void MapUser(EntityTypeBuilder<User> entityTypeBuilder) {
            entityTypeBuilder.HasKey(x => x.Id);
            entityTypeBuilder.Property(x => x.FirstName).IsRequired().HasMaxLength(200);
            entityTypeBuilder.Property(x => x.LastName).IsRequired().HasMaxLength(200);
        }
        public static void MapLocalization(EntityTypeBuilder<Culture> entityTypeBuilderCulture,EntityTypeBuilder<Resource> entityTypeBuilderResource)
        {
            entityTypeBuilderCulture.HasKey(x => x.Id);
            //seed data
            entityTypeBuilderCulture.HasData(
              new Culture("en-US") { Name = "English (US)", IsDefault = true },
              new Culture("vi-VN") { Name = "Tiếng Việt (VN)", IsDefault = false }
           );
            entityTypeBuilderResource.HasData(
                new Resource(){Id=1, CultureId = "vi-VN", Value= "Blog Đơn Giản",Key= "Blog Simple"},
                new Resource(){ Id = 2, CultureId = "vi-VN", Value = "Thoát", Key = "Logout" },
                new Resource() { Id = 3, CultureId = "vi-VN", Value = "Đăng Nhập", Key = "Login" },
                new Resource() { Id = 4, CultureId = "vi-VN", Value = "Tìm Kiếm", Key = "Search" },
                new Resource() { Id = 5, CultureId = "vi-VN", Value = "Đi", Key = "Search" },
                new Resource() { Id = 6, CultureId = "vi-VN", Value = "Tìm Kiếm", Key = "Go" },
                new Resource() { Id = 7, CultureId = "vi-VN", Value = "Loại", Key = "Categories" },
                new Resource() { Id = 8, CultureId = "vi-VN", Value = "Chào", Key = "Hello" },
                new Resource() { Id = 9, CultureId = "vi-VN", Value = "Bình Luận Của Bạn", Key = "Your Comment" },
                new Resource() { Id = 10, CultureId = "vi-VN", Value = "Tổng Bình Luận", Key = "Total Comment(s)" },
                new Resource() { Id = 11, CultureId = "vi-VN", Value = "Tác Giả", Key = "Author" },
                new Resource() { Id = 12, CultureId = "vi-VN", Value = "Email thì không tồn tại", Key = "Email is not exist" },
                new Resource() { Id = 13, CultureId = "vi-VN", Value = "Email thì không hợp lệ", Key = "The email is invalid" },
                new Resource() { Id = 14, CultureId = "vi-VN", Value = "Tạo Người Dùng", Key = "Create User" },
                new Resource() { Id = 15, CultureId = "vi-VN", Value = "Email thì bắt buộc", Key = "The Email field is required" },
                new Resource() { Id = 16, CultureId = "vi-VN", Value = "Đăng ký", Key = "Register" }
                );
        }
        
    }
}
 