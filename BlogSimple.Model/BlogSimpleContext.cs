﻿using BlogSimple.Model.ConfigMap;
using BlogSimple.Model.Domain;
using Microsoft.EntityFrameworkCore;

namespace BlogSimple.Model
{
    public class BlogSimpleContext: DbContext
    {
        public BlogSimpleContext(DbContextOptions<BlogSimpleContext> options):base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ConfigMaps.MapBlogPost(modelBuilder.Entity<BlogPost>());
            ConfigMaps.MapCategory(modelBuilder.Entity<Category>());
            ConfigMaps.MapComment(modelBuilder.Entity<Comment>());
            ConfigMaps.MapUser(modelBuilder.Entity<User>());
            ConfigMaps.MapLocalization(modelBuilder.Entity<Culture>(), modelBuilder.Entity<Resource>());
        }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Culture> Culture { get; set; }
        public DbSet<Resource> Resource { get; set; }
    }
}
