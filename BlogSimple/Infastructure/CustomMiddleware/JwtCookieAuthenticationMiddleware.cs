﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace BlogSimple.Infastructure.CustomMiddleware
{
    public class JwtCookieAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        public static string LoginPagePath { get; set; }
        public JwtCookieAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"];
            await _next(context);
            if (context.Response.StatusCode == 401)
            {
                    context.Response.Redirect(LoginPagePath);
            }
            
        }
    }
    public static class JWTCookieAuthMiddlewareExtensions
    {
        public static IApplicationBuilder EnableJwtCookieAuthentication(this IApplicationBuilder app, string loginPagePath)
        {
            JwtCookieAuthenticationMiddleware.LoginPagePath = loginPagePath;

            return app.UseMiddleware<JwtCookieAuthenticationMiddleware>();
        }
    }
    public class TokenAuthOption
    {
        public static string Audience { get; } = "ExampleAudience";
        public static string Issuer { get; } = "ExampleIssuer";
        public static RsaSecurityKey Key { get; } = new RsaSecurityKey(RSAKeyHelper.GenerateKey());
        public static SigningCredentials SigningCredentials { get; } = new SigningCredentials(Key, SecurityAlgorithms.RsaSha256Signature);

        public static TimeSpan ExpiresSpan { get; } = TimeSpan.FromMinutes(20);
    }
    public class RSAKeyHelper
    {
        public static RSAParameters GenerateKey()
        {
            using (var key = new RSACryptoServiceProvider(2048))
            {
                return key.ExportParameters(true);
            }
        }
    }
}
