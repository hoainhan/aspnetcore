﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Infastructure.CustomMiddleware
{
    public class NoCacheMiddleware
    {
        private readonly RequestDelegate _next;
        public NoCacheMiddleware (RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            if(!httpContext.User.Identity.IsAuthenticated)
            {
                httpContext.Response.OnStarting((state) =>
                {
                    httpContext.Response.Headers.Append("Cache-Control", "no-cache, no-store, must-revalidate");
                    httpContext.Response.Headers.Append("Expires", "0");
                    return Task.FromResult(0);
                }, null);
            }
            await _next.Invoke(httpContext);

        }
    }
    public static class NoCacheMiddlewareExtensions
    {
        public static IApplicationBuilder NoCacheAfterLogin(this IApplicationBuilder app)
        {
            return app.UseMiddleware<NoCacheMiddleware>();
        }
    }
}
