﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogSimple.Infastructure
{
    [HtmlTargetElement("comment")]
    public class CommentTagHelper : TagHelper
    {
        [HtmlAttributeName("rows")]
        public int Rows { get; set; }

        [HtmlAttributeName("enable-send")]
        public bool EnableSend { get; set; }

        [HtmlAttributeName("controller")]
        public string Controller { get; set; }

        [HtmlAttributeName("action")]
        public string Action { get; set; }

        [HtmlAttributeName("method")]
        public string Method { get; set; }

        [HtmlAttributeName("IdPage")]
        public string IdPage { get; set; }

        [HtmlAttributeName("ValueIdPage")]
        public Guid ValueIdPage { get; set; }

        public override void Process(
            TagHelperContext context,
            TagHelperOutput output)
        {
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("class", "div-comment");
            var sb = new StringBuilder();
            sb.AppendFormat("<form method='{0}' action='/{1}/{2}'>",Method, Controller, Action );
            sb.AppendFormat("<textarea rows='{0}' name='comment' class='{1}'></textarea> ", Rows, "form-control margin-bottom-10");
            sb.AppendFormat("<input type='hidden' value='{0}' name='{1}' />", ValueIdPage, IdPage);
            if(this.EnableSend)
            {
                sb.AppendFormat("<button class='{0}' id='btnPost' type='submit'>Send Comment</button>", "btn btn-success");
            }
            sb.AppendFormat("{0}", "</form>");
            output.Content.AppendHtml(sb.ToString());
            base.Process(context,output);
        }
    }

}
