﻿using AutoMapper;
using BlogSimple.Areas.Admin.Models;
using BlogSimple.Model.Domain;
using BlogSimple.Models;

namespace BlogSimple.Infastructure.Mapper
{
    public class ConfigMapper:Profile
    {
        public ConfigMapper()
        {
            CreateMap<Category, CategoryModel>().ReverseMap();
            CreateMap<BlogPost, PostModel>().ReverseMap();
            CreateMap<User, UserModel>().ReverseMap();
        }
    }

}
