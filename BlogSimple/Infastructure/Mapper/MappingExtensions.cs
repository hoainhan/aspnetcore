﻿using AutoMapper;
using BlogSimple.Areas.Admin.Models;
using BlogSimple.Model.Domain;
using BlogSimple.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Infastructure.Mapper
{
    public static class MappingExtensions
    {
        public static TDestination Mapto<TSource, TDestination>(this TSource source)
        {
            return AutoMapper.Mapper.Map<TSource, TDestination>(source);
        }
        //category
        public static Category ToEntiy(this CategoryModel categoryModel)
        {
            return categoryModel.Mapto<CategoryModel, Category>();
        }

        public static CategoryModel ToModel(this Category category)
        {
            return category.Mapto<Category, CategoryModel>();
        }
        //post
        public static BlogPost ToEntiy(this PostModel postModel)
        {
            return postModel.Mapto<PostModel, BlogPost>();
        }

        public static PostModel ToModel(this BlogPost post)
        {
            return post.Mapto<BlogPost, PostModel>();
        }
        //user
        public static User ToEntiy(this UserModel userModel)
        {
            return userModel.Mapto<UserModel, User>();
        }

        public static UserModel ToModel(this User user)
        {
            return user.Mapto<User, UserModel>();
        }
    }
}
