﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using BlogSimple.Repository.Localization;
using System.Globalization;
using Microsoft.AspNetCore.Localization;

namespace BlogSimple.Helpers
{
    public static class ExtensionHelper
    {
        /// <summary>
        /// extention for config
        /// </summary>
        /// <typeparam name="TConfig"></typeparam>
        /// <param name="serviceCollection"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static TConfig ConfigureStartupConfig<TConfig>(this IServiceCollection serviceCollection,
            IConfiguration configuration) where TConfig : class, new()
        {
            if (serviceCollection == null)
                throw new ArgumentNullException(nameof(serviceCollection));
            if(configuration ==null)
                throw new ArgumentNullException(nameof(configuration));
            var config = new TConfig();

            // bind all setting from appsetting.json
            configuration.Bind(config);
            //register singleton
            serviceCollection.AddSingleton(config);

            return config;
        }
        public static IApplicationBuilder UseCustomizedRequestLocalization(this IApplicationBuilder app)
        {
            var cultureRepository = app.ApplicationServices.GetRequiredService<ICultureRepository>();
            var cultures = cultureRepository.GetAll().ToList();

            GlobalConfiguration.SimpleCultures = cultures.Select(x => new SimpleCulture { Id = x.Id, Name = x.Name }).ToList();
            var supportedCultures = cultures.Select(x => new CultureInfo(x.Id)).ToList();
            var defaultCulture = cultures.Where(x => x.IsDefault).Select(x => new CultureInfo(x.Id)).FirstOrDefault();
            if (defaultCulture == null)
            {
                defaultCulture = new CultureInfo("en-US");
            }

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(defaultCulture, defaultCulture),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
            return app;
        }
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            var test = request.Headers["AngularRequested"];
            return request.Headers["AngularRequested"] == "True";
        }


    }
}
