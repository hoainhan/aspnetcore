﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Helpers
{
    public class BlogSimpleConfig
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
    }
    public static class GlobalConfiguration
    {
        public static IList<SimpleCulture> SimpleCultures { get; set; } = new List<SimpleCulture>();
    }
    public class SimpleCulture
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
