﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Infastructure.CustomAttribute
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class UserNameValidate: ValidationAttribute, IClientModelValidator
    {
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userName = value.ToString();
            if(userName.IndexOf(' ') != -1)
            {
                return new ValidationResult("UserName can't contain space character");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
