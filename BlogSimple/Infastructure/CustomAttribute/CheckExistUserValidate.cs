﻿using BlogSimple.Cache;
using BlogSimple.Repository.UserRepo;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace BlogSimple.Infastructure.CustomAttribute
{
    public class CheckExistUserValidate:ValidationAttribute
    {
        public new string ErrorMessage { get; set; }
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if(value !=null)
            {
                var repoUser = (IUserRepository)context.GetService(typeof(IUserRepository));
                var getCacheUserId = MyCache.GetCacheCommon!=null? MyCache.GetCacheCommon.UserId : Guid.Empty;
                var getCacheEmail = MyCache.GetCacheCommon != null ? MyCache.GetCacheCommon.Email : "";
                if (repoUser.where(x=>(x.UserName == value.ToString() || x.Email == getCacheEmail) && x.Id != getCacheUserId).ToList().Count > 0)
                {
                    return new ValidationResult(this.ErrorMessage);
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return base.IsValid(value, context);
        }
    }
}
