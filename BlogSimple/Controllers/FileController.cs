﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogSimple.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogSimple.Controllers
{
    public class FileController : Controller
    {
        private IBlogPostService _blogPostService;
        public FileController(IBlogPostService blogPostService)
        {
            _blogPostService = blogPostService;
        }
        [HttpGet]
        public FileContentResult GetImageForPost(Guid id)
        {
            try
            {
                var post = this._blogPostService.GetById(id);
                return File(post.ThumbnailImage, "image/png");
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}