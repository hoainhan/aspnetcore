﻿using BlogSimple.Cache;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Model.Domain;
using BlogSimple.Models;
using BlogSimple.Repository;
using BlogSimple.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BlogSimple.Controllers
{
    public class PostController : BaseController
    {
        private IBlogPostRepository _blogPostRepository;
        private IBlogPostService _blogPostService;
        
        public PostController(IBlogPostRepository blogPostRepository, IBlogPostService blogPostService)
        {
            _blogPostRepository = blogPostRepository;
            _blogPostService = blogPostService;
            
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetDetail(Guid id)
        {
            var data = _blogPostRepository.GetById(id).ToModel();
            return View(data);
        }
        public IActionResult Search(string keysearch, Guid categoryId)
        {
            ViewBag.keySearch = keysearch; 
            return View("Search",new SearchKeyListPostModel { CategoryId=categoryId, KeySearch=keysearch});
        }
        [HttpPost]
        public IActionResult Comment(string comment,Guid postId)
        {
            if(!string.IsNullOrEmpty(comment))
            {
                var useriD = MyCache.GetCacheCommon.UserId;
                var ob = new Comment { Id = Guid.NewGuid(), CommentDescription = comment, BlogPostId = postId,UpdatedBy = useriD,
                    CreatedBy =useriD,UpdatedDate=DateTime.Now,CreatedDate=DateTime.Now };

                var getBlogPost = _blogPostRepository.GetById(postId);

                if (getBlogPost.Comments == null)
                    getBlogPost.Comments = new List<Comment>();

                getBlogPost.Comments.Add(ob);

                _blogPostRepository.Update(getBlogPost);
                Save();
            }
            return RedirectToAction("GetDetail", new { id=postId});
        } 
        [HttpGet]
        public FileContentResult GetImageForPost(Guid id)
        {
            try
            {
                var post = this._blogPostService.GetById(id);
                return File(post.ThumbnailImage, "image/png");
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

}