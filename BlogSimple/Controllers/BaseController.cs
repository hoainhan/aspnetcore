﻿using BlogSimple.Repository.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace BlogSimple.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
           
        }
       
        public void Save()
        {
            var unitOfWork = (IUnitOfWork)HttpContext.RequestServices.GetService(typeof(IUnitOfWork));
            unitOfWork.Commit();
        }

    }
}