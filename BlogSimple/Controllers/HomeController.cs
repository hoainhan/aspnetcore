using BlogSimple.Cache;
using BlogSimple.Helpers;
using BlogSimple.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace BlogSimple.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IBlogPostService _authorService;
        private readonly BlogSimpleConfig _blogSimpleConfig;
        private static IMemoryCache _memoryCache;
        readonly ILogger<HomeController> _logger;
        public HomeController(IBlogPostService authorService, BlogSimpleConfig blogSimpleConfig, IMemoryCache memoryCache,
            ILogger<HomeController> logger)
        {
            _authorService = authorService;
            _blogSimpleConfig = blogSimpleConfig;
            _memoryCache = memoryCache;
            _logger = logger;
        } 

        public IActionResult Index()
        {
            //init cache
            MyCache._memoryCache = _memoryCache;

            return View();
        }
        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
        public IActionResult Contact()
        {
            return Redirect("/Test");
        }
    }
}
