﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogSimple.Infastructure.CustomConvention;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogSimple.Controllers
{
    [RabbitController]
    public class TestController : Controller
    {
        public JsonResult GetFile()
        {
            return Json("abc");
        }
    }
}