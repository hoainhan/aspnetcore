﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Models
{
    public class SearchKeyListPostModel
    {
        public string KeySearch { get; set; }
        public Guid CategoryId { get; set; }
    }
}
