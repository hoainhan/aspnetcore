﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Models
{
    public class ListPostModel
    {
        public Guid Id { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
    }
}
