﻿using BlogSimple.Infastructure.CustomAttribute;
using BlogSimple.Repository.UserRepo;
using Microsoft.Extensions.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlogSimple.Models
{
    public class UserModel
    {
       
        public Guid Id { get; set; }
        [Required]
        [MinLength(5)]
        [UserNameValidate]
        [CheckExistUserValidate(ErrorMessage ="UserName is exist")]
        [Display(Name ="UserName")]
        public string UserName { get; set; }

        [Required]
        [Display(Name ="Email")]
        [EmailAddress(ErrorMessage = "The email is invalid")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [MinLength(6)]
        [Compare("CPassword", ErrorMessage ="Password and Confirm Password is not same ")]
        public string Password { get; set; }

        [Required]
        [Display(Name ="Confirm Password")]
        [MinLength(6)]
        [Compare("Password", ErrorMessage = "Password and Confirm Password is not same ")]
        public string CPassword { get; set; }

        public string PasswordSalt { get; set; }
    }
}
