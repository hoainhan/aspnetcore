using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BlogSimple.Controllers;
using BlogSimple.Helpers;
using BlogSimple.Infastructure.CustomMiddleware;
using BlogSimple.Localization;
using BlogSimple.Model;
using BlogSimple.Modules;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace BlogSimple
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //Mapper
            services.AddAutoMapper();
            //Add Mvc
            services.AddMvc(option=> {
                   // option.Conventions.Add(new RabbitConvention());
            }).AddViewLocalization().AddMvcLocalization()
            .AddDataAnnotationsLocalization(option=> {
                option.DataAnnotationLocalizerProvider = (type, factory) =>
                factory.Create(null);
            });
            services.AddSession(option=> {});

            //Add Cookie Authentication
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).
                AddCookie(option =>
                {
                    option.LoginPath = "/Login";
                    option.AccessDeniedPath = "/Login";
                    option.ExpireTimeSpan = new TimeSpan(20, 0, 0, 0);
                })
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateActor = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["BlogSimple:Issuer"],
                        ValidAudience = Configuration["BlogSimple:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes
                                                           (Configuration["BlogSimple:Key"]))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy => policy.RequireClaim("Admin"));
                //jwt authentication
                options.AddPolicy("Bearer", new AuthorizationPolicyBuilder().AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            })
            ;
            //translate
            services.AddSingleton<IStringLocalizerFactory, EfStringLocalizerFactory>();
         

            //add connection string
            services.AddDbContext<BlogSimpleContext>(options => options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            //Add MemoryCache
            services.AddMemoryCache();

           //Load appsetting to blogsimple config
            services.ConfigureStartupConfig<BlogSimpleConfig>(Configuration.GetSection("BlogSimple"));

            //Config DI & IOC
            return new AutofacServiceProvider(RegisterModules.RegisterModule(services));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseCustomizedRequestLocalization();
            app.UseStaticFiles();

            app.UseCors(builder =>
            builder.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());


            app.UseAuthentication();
            app.EnableJwtCookieAuthentication("/Login");

           // app.NoCacheAfterLogin();

            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Admin}/{action=Index}/{id?}"
                  );

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Admin", action = "Index" });
            });

            
        }
    }
}
