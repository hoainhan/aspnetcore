﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogSimple.Service;
using BlogSimple.Areas.Admin.Models;
using BlogSimple.Infastructure.Mapper;
using System.IO;
using BlogSimple.Cache;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogSimple.Areas.Admin.Controllers
{

    [Route("api/post/[action]")]
    public class PostController : Controller
    {
        #region Constructor
        private IBlogPostService _blogPostService;
        public PostController(IBlogPostService blogPostService)
        {
            _blogPostService = blogPostService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public List<PostModel> GetAll()
        {
            var userId = MyCache.GetCacheCommon.UserId;
            return this._blogPostService.GetAll().Where(x=>x.CreatedBy ==userId).Select(x => x.ToModel()).ToList();
        }
        [HttpPost]
        public bool Create([FromForm] PostModel postModel)
        {
            if (ModelState.IsValid)
            {
                if (postModel.Image != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        postModel.Image.CopyTo(memoryStream);
                        postModel.ThumbnailImage = memoryStream.ToArray();
                    }
                }
                return this._blogPostService.Create(postModel.ToEntiy()) != null;
            }
            return false;
        }
        [HttpPost]
        public bool Update([FromForm] PostModel postModel)
        {
            if (ModelState.IsValid)
            {
                return this._blogPostService.UpdateCustom(postModel.ToEntiy()) != null;
            }
            return false;
        }
        [HttpPost]
        public PostModel GetById([FromBody]CommonModel commonModel)
        {
            return this._blogPostService.GetById(commonModel.Id).ToModel();
        }
        public bool Delete([FromBody] CommonModel commonModel)
        {
            if (ModelState.IsValid)
            {
                var post = this._blogPostService.GetById(commonModel.Id);
                this._blogPostService.Delete(post);
            }
            return true;
        }
        [HttpGet("{id}")]
        public FileContentResult GetImage(Guid Id)
        {
            try
            {
                var post = this._blogPostService.GetById(Id);
                return File(post.ThumbnailImage, "image/png");
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion
    }
}
