﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Models;
using BlogSimple.Service;
using Microsoft.AspNetCore.Mvc;
using BlogSimple.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using BlogSimple.Cache;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogSimple.Areas.Admin.Controllers
{
    [Authorize(Policy="Bearer")]
    [Route("api/category/[action]")]
    public class CategoryController : BaseAdminController
    {
        #region Constructor
        public ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService) {
            _categoryService = categoryService;
        }
        #endregion

        #region Methods
        [HttpPost]
        public bool Create([FromBody] CategoryModel categoryModel)
        {
            if(ModelState.IsValid)
            {
                return this._categoryService.Create(categoryModel.ToEntiy()) !=null;
            }
            return false;
        }
        [HttpPost]
        public bool Update([FromBody] CategoryModel categoryModel)
        {
            if (ModelState.IsValid)
            {
                return this._categoryService.UpdateCategory(categoryModel.ToEntiy()) != null;
            }
            return false;
        }
        [HttpPost]
        public bool Delete([FromBody] CommonModel commonModel)
        {
            if (ModelState.IsValid)
            {
                var category = this._categoryService.GetById(commonModel.Id);
                 this._categoryService.Delete(category);
            }
            return true;
        }
        [HttpGet]
        public List<CategoryModel> GetAll()
        {
            var userId = MyCache.GetCacheCommon.UserId;
            return this._categoryService.GetAll().Where(x=>x.CreatedBy == userId).Select(x=>x.ToModel()).ToList();
        }
        [HttpPost]
        public CategoryModel GetById([FromBody]CommonModel commonModel)
        {
            return this._categoryService.GetById(commonModel.Id).ToModel();
        }
        #endregion

    }
}
