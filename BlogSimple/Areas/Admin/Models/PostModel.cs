﻿using BlogSimple.Model.Domain;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogSimple.Areas.Admin.Models
{
    public class PostModel
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string Content { get; set; }
        public IFormFile Image { get; set; }
        public byte[] ThumbnailImage { get; set; }
        public List<Comment> Comments { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
