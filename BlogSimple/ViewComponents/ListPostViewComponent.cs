﻿using BlogSimple.Model;
using BlogSimple.Models;
using BlogSimple.Repository;
using BlogSimple.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.ViewComponents
{
    public class ListPostViewComponent : ViewComponent
    {
        //can inject any service/repo
        private readonly IBlogPostRepository _blogPostRepository;
        public ListPostViewComponent(IBlogPostRepository blogPostRepository) {
            _blogPostRepository = blogPostRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(string keysearch, Guid categoryId)
        {  
            var data = new List<ListPostModel>();
            if(categoryId !=null && categoryId != Guid.Empty)
            {
                var result = await _blogPostRepository.GetAllAsync();
                  data=  result.Where(x => x.CategoryId == categoryId).OrderByDescending(x=>x.CreatedDate).
                  Select(x => new ListPostModel { Content = x.Content, ShortDescription = x.ShortDescription, Id = x.Id }).ToList();
            }
            else
            {
                if (string.IsNullOrEmpty(keysearch))
                    keysearch = "";
                 keysearch = keysearch.ToLower();
                var result = await _blogPostRepository.GetAllAsync();
                data = result.OrderByDescending(x=>x.CreatedDate).Where(x => x.ShortDescription.ToLower().Contains(keysearch) || x.Content.ToLower().Contains(keysearch) || keysearch == "").
                 Select(x => new ListPostModel { Content = x.Content, ShortDescription = x.ShortDescription, Id = x.Id }).ToList();
            }
            
            return View(data);
        }  
    }
}
