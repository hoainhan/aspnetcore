﻿using BlogSimple.Models;
using BlogSimple.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.ViewComponents
{
    public class ListCategoryViewComponent:ViewComponent
    {
        private readonly ICategoryRepository _categoryRepository;
        public ListCategoryViewComponent(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _categoryRepository.GetAllAsync();
             var result =  data.Select(x=> new CategoryModel { Description= x.Description, Name = x.Name, Id=x.Id});
            return View(result.ToList());
        }
    }
}
