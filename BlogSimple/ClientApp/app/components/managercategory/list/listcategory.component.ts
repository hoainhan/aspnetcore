﻿import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { CategoryFilterPipe } from '../../../directives/category.pipe'
import {CategoryService } from '../../../services/category.service'
import { ICategory } from '../../../models/ICategory';
import { ShareService } from '../../../services/shareservice'
import { DBOperation } from '../../../models/enum';

@Component({
  selector: "list-category-component",
  styles: ["listcategory.component.css"],
  templateUrl: "listcategory.component.html"
})

export class ListCategoryComponent implements OnInit {
    
    isREADONLY: boolean = false;
    exportFileName: string = "Category_";

    categories: ICategory[];
    msg: string;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;

    //Grid Vars start
    columns: any[] = [
        {
            display: 'Name',
            variable: 'name',
            filter: 'text',
        },
        {
            display: 'Description',
            variable: 'description',
            filter: 'text'
        }
    ];
    sorting: any = {
        column: 'Name',
        descending: false
    };
    hdrbtns: any[] = [];
    gridbtns: any[] = [];
    initGridButton() {

        this.hdrbtns = [
            {
                title: 'Add',
                keys: [''],
                action: DBOperation.create,
                ishide: this.isREADONLY

            }];
        this.gridbtns = [
            {
                title: 'Edit',
                keys: ['id'],
                action: DBOperation.update,
                ishide: this.isREADONLY
            },
            {
                title: 'X',
                keys: ["id"],
                action: DBOperation.delete,
                ishide: this.isREADONLY
            }

        ];

    }
    constructor(private categoryService: CategoryService, private router: Router, private shareService: ShareService) {
        this.initGridButton();
    }
    ngOnInit(): void {
        this.categoryService.getlist().subscribe(result => {
            this.categories = result;
        });

    }
    gridaction(gridaction: any): void {

        switch (gridaction.action) {
            case DBOperation.create:
                this.addCategory();
                break;
            case DBOperation.update:
              this.editCategory(gridaction.values[0].value);
                break;
            case DBOperation.delete:
              this.deleteCategory(gridaction.values[0].value);
                break;
        }
    }
    addCategory() {
      this.router.navigate(['managercategory', 'create']);
    }
    editCategory(id: string) {
      this.router.navigate(['managercategory', 'edit', id]);
    }
    deleteCategory(id: string)
    {
      var confirmDelete = confirm("Are you sure to delete ?");
      if (confirmDelete)
      {
        this.categoryService.Delete(id).subscribe(result => {
          if (result) {
            this.shareService.publishShowMessage({ type: 1, message: "Delete Successfully.", date: new Date() });
            this.categories = this.categories.filter(x => x.id != id);
          }
          else {
            this.shareService.publishShowMessage({ type: 2, message: "Delete Failed.", date: new Date() });
          }
        });
      }

    }
}
