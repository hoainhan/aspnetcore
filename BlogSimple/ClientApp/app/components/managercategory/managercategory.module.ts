﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { CreateCategoryComponent } from './create/createcategory.component';
import { ListCategoryComponent } from './list/listcategory.component';

import { CategoryRouterModule } from './managercategory-router.module';
import { FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';
import { AppPipeDirectiveModule } from '../../app.pipedirective.module'

@NgModule({
  imports: [
    CommonModule,
    AppPipeDirectiveModule,
      FormsModule, ReactiveFormsModule,
         CategoryRouterModule
  ],
  declarations: [
     CreateCategoryComponent,
        ListCategoryComponent
    ]
})
export class CategoryModule { }