﻿import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { AuthGuard } from '../auth-guard.service';
import { ListCategoryComponent } from './list/listcategory.component'
import { CreateCategoryComponent } from './create/createcategory.component'

const managercategoryRouters: Routes = [{
  path: '',
  component: ListCategoryComponent,
  canActivate: [AuthGuard],
  },
  {
    path: 'create',
    component: CreateCategoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: CreateCategoryComponent,
    canActivate: [AuthGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(managercategoryRouters)],
  exports: [RouterModule]
})
  export class CategoryRouterModule {}