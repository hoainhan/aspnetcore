﻿import { Component, OnInit, Inject } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'
import { ValidationService } from '../../../services/validation.service';
import { CategoryService } from '../../../services/category.service';
import { ICategory } from "../../../models/ICategory";
import { ShareService } from "../../../services/shareservice"
import { Guid } from "guid-typescript";
@Component({
    selector: "create-category-component",
    styleUrls: ["createcategory.component.css"],
    templateUrl: "createcategory.component.html"
})

export class CreateCategoryComponent implements OnInit {

     createCategoryForm: FormGroup;
     title: string = "Create Category";
     categoryModel: ICategory;
     constructor(private formBuilder: FormBuilder, private categoryService: CategoryService,
       private router: Router, private activateRouter: ActivatedRoute, private shareService: ShareService) {
       var id = this.activateRouter.snapshot.paramMap.get('id');
       if (id != null && id != undefined)
       {
         this.title = "Edit Category";
         this.categoryService.getbyid(id).subscribe(result => { this.categoryModel = result; this.bindData(); })
       }
      
        
    }
     ngOnInit(): void {
       this.createCategoryForm = this.formBuilder.group({
         'id': [Guid.create().toString()],
         'name': ['', Validators.required],
         'description': ['', [Validators.required, Validators.minLength(5)]]
       });

     }
    saveCategory() {
      if (this.createCategoryForm.valid) {
        if (this.categoryModel != null)
        {
          this.categoryService.Update(this.createCategoryForm.value).subscribe(result => {
            if (result) { this.router.navigate(['/', 'managercategory']) }
            else { this.shareService.publishShowMessage({ type: 1, message: "Save Failed.", date: new Date() }); }
          });
        }
        else
        {
          this.categoryService.Create(this.createCategoryForm.value).subscribe(result => {
            if (result) { this.router.navigate(['/', 'managercategory']) }
            else { this.shareService.publishShowMessage({ type: 1, message: "Save Failed.", date: new Date() });}
          });
        }
         
        }
     }
    bindData() {
      this.createCategoryForm.controls['name'].setValue(this.categoryModel.name);
      this.createCategoryForm.controls['description'].setValue(this.categoryModel.description);
      this.createCategoryForm.controls['id'].setValue(this.categoryModel.id);
    }
}