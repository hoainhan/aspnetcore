﻿import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CreateBlogpostComponent } from './create/createblogpost.component'
import { ListBlogpostComponent } from './list/listblogpost.component'
import { AuthGuard } from '../auth-guard.service';

const blogpostroutes: Routes = [{
    path: '',
    component: ListBlogpostComponent,
    canActivate: [AuthGuard]
    },
    {
        path: 'create',
        component: CreateBlogpostComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: CreateBlogpostComponent,
        canActivate: [AuthGuard]
    }
];
@NgModule({
    imports: [RouterModule.forChild(blogpostroutes)],
    exports: [RouterModule]
})
export class BlogPostRouterModule { }