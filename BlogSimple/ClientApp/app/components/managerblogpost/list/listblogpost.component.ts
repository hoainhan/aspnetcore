﻿import { Component, OnInit } from '@angular/core'
import { BlogPostService } from '../../../services/blogpost.service'
import { Router } from '@angular/router';
import { ShareService } from '../../../services/shareservice';
import { DBOperation } from '../../../models/enum';
import { IPost } from '../../../models/IPost';

@Component({
    selector: 'list-blogpost',
    templateUrl: 'listblogpost.component.html',
    styles: ['listblogpost.component.scss']
})
export class ListBlogpostComponent implements OnInit {

    isREADONLY: boolean = false;
    exportFileName: string = "Post_";

    posts: IPost[];
    msg: string;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;

    //Grid Vars start
    columns: any[] = [
        {
            display: 'Short Description',
            variable: 'shortDescription',
            filter: 'text',
        },
        {
            display: 'Content',
            variable: 'content',
            filter: 'text'
        }
    ];
    sorting: any = {
        column: 'shortDescription',
        descending: false
    };
    hdrbtns: any[] = [];
    gridbtns: any[] = [];
    initGridButton() {

        this.hdrbtns = [
            {
                title: 'Add',
                keys: [''],
                action: DBOperation.create,
                ishide: this.isREADONLY

            }];
        this.gridbtns = [
            {
                title: 'Edit',
                keys: ['id'],
                action: DBOperation.update,
                ishide: this.isREADONLY
            },
            {
                title: 'X',
                keys: ["id"],
                action: DBOperation.delete,
                ishide: this.isREADONLY
            }

        ];

    }
    constructor(private postService: BlogPostService, private router: Router, private shareService: ShareService) {
        this.initGridButton();
    }
    ngOnInit(): void {
        this.postService.getlist().subscribe(result => {
            this.posts = result;
        });

    }
    gridaction(gridaction: any): void {

        switch (gridaction.action) {
            case DBOperation.create:
                this.addPost();
                break;
            case DBOperation.update:
                this.editPost(gridaction.values[0].value);
                break;
            case DBOperation.delete:
                this.deletePost(gridaction.values[0].value);
                break;
        }
    }
    addPost() {
        this.router.navigate(['managerblogpost', 'create']);
    }
    editPost(id: string) {
      this.router.navigate(['managerblogpost', 'edit', id]);
    }
    deletePost(id: string) {
        var confirmDelete = confirm("Are you sure to delete ?");
        if (confirmDelete) {
            this.postService.Delete(id).subscribe(result => {
                if (result) {
                    this.shareService.publishShowMessage({ type: 1, message: "Delete Successfully.", date: new Date() });
                    this.posts = this.posts.filter(x => x.id != id);
                }
                else {
                    this.shareService.publishShowMessage({ type: 2, message: "Delete Failed.", date: new Date() });
                }
            });
        }

    }
}
