﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogPostRouterModule } from './blogpost-router.module'
import { FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';
import { CreateBlogpostComponent } from './create/createblogpost.component'
import { ListBlogpostComponent } from './list/listblogpost.component'
import { AppPipeDirectiveModule } from '../../app.pipedirective.module'


@NgModule({
  imports: [CommonModule, AppPipeDirectiveModule, FormsModule, ReactiveFormsModule, BlogPostRouterModule]
    ,
    declarations: [CreateBlogpostComponent, ListBlogpostComponent]

})
export class BlogPostModule { }