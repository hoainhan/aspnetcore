﻿import { Component, Inject } from '@angular/core'
import { ShareService } from "../../../services/shareservice"
import { Guid } from "guid-typescript";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { IPost } from "../../../models/IPost";
import { BlogPostService } from "../../../services/blogpost.service";
import { Router,ActivatedRoute } from "@angular/router";
import { CategoryService } from "../../../services/category.service";
@Component({
    selector: 'create-blogpost',
    templateUrl: 'createblogpost.component.html',
    styles: ['createblogpost.component.scss']
})
export class CreateBlogpostComponent {
  postForm: FormGroup;
    title: string = "Create Post";
    titleImage: string = "Upload";
    url: string;
    postModel: IPost;
    temp: string = new Date().getTime().toString(); //-- for refresh image
    catogories = new Array();
    constructor(private formBuilder: FormBuilder, private postService: BlogPostService, @Inject('BASE_URL') baseUrl: string,
        private router: Router, private activateRouter: ActivatedRoute, private shareService: ShareService, private categoryService: CategoryService) {
        this.url = baseUrl;
        var id = this.activateRouter.snapshot.paramMap.get('id');
        if (id != null && id != undefined) {
            this.title = "Edit Post";
            this.titleImage = "Change Image";
            this.postService.getbyid(id).subscribe(result => { this.postModel = result; this.bindData(); })
        }
        this.categoryService.getlist().subscribe(result => { this.catogories = result;});
  }
    ngOnInit(): void {
    this.postForm = this.formBuilder.group({
      'id': [Guid.create().toString()],
      'content': ['', Validators.required],
      'shortDescription': ['', [Validators.required, Validators.minLength(5)]],
      'categoryId': ['', [Validators.required]],
      'image':[null]
    });

    }

    fileChange(files: FileList) {
        if (files && files[0].size > 0) {
            this.postForm.patchValue({
                image: files[0]
            });
        }
    }
  savePost() {
      if (this.postForm.valid) {

          var formModel = this.postForm.value;

          var formData = new FormData();
          formData.append("id", formModel.id);
          formData.append("content", formModel.content);
          formData.append("shortDescription", formModel.shortDescription);
          formData.append("image", formModel.image);
          formData.append("categoryId", formModel.categoryId);

        if (this.postModel != null) {
            this.postService.Update(formData).subscribe(result => {
          if (result) { this.router.navigate(['/', 'managerblogpost']) }
          else { this.shareService.publishShowMessage({ type: 1, message: "Save Failed.", date: new Date() }); }
        });
      }
        else {
            this.postService.Create(formData).subscribe(result => {
            if (result) { this.router.navigate(['/', 'managerblogpost']) }
          else { this.shareService.publishShowMessage({ type: 1, message: "Save Failed.", date: new Date() }); }
        });
      }

    }
    }
    getImage() {
        if (this.postModel != null)
            return this.url + "api/post/GetImage/" + this.postModel.id + "?t=" + this.temp;
    }
  bindData() {
      this.postForm.controls['content'].setValue(this.postModel.content);
      this.postForm.controls['shortDescription'].setValue(this.postModel.shortDescription);
      this.postForm.controls['id'].setValue(this.postModel.id);
      this.postForm.controls['categoryId'].setValue(this.postModel.categoryId);
  }
}