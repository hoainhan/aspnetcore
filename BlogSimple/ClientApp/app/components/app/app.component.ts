import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart } from '@angular/router'
import { ShareService } from '../../services/shareservice'
import { IShowMessage } from "../../models/IShowMessage";
import 'rxjs/add/operator/filter';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  public showMessage: IShowMessage;
  constructor(private shareService: ShareService, private router: Router) {
    this.shareService.showMessage$.subscribe(result => { this.showMessage = result });

    router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        this.closeAlert();
      });
  }
  getClassMessage():string {
    if (this.showMessage != null)
    {
      if (this.showMessage.type == 1)
      {
        return "alert-success";
      }
      if (this.showMessage.type == 2) {
        return "alert-danger";
      }
    }
    return "";
  }
  closeAlert() {
    this.showMessage = null;
  }
}
