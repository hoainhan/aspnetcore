﻿import { PipeTransform, Pipe, Injectable } from '@angular/core';
import { ICategory } from '../models/ICategory';



@Pipe({
    name: 'categoryFilter'
})

@Injectable()
export class CategoryFilterPipe implements PipeTransform {

    transform(value: ICategory[], filter: string): ICategory[] {
        filter = filter ? filter.toLocaleLowerCase() : null;
        return filter ? value.filter((app: ICategory) =>
          app.name != null && app.name.toLocaleLowerCase().indexOf(filter) != -1
            || app.description != null && app.description.toLocaleLowerCase().indexOf(filter) != -1
        ) : value;
    }

}