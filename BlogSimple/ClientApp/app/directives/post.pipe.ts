﻿import { PipeTransform, Pipe, Injectable } from '@angular/core';
import { IPost } from '../models/IPost';



@Pipe({
    name: 'postFilter'
})

@Injectable()
export class PostFilterPipe implements PipeTransform {

    transform(value: IPost[], filter: string): IPost[] {
        filter = filter ? filter.toLocaleLowerCase() : null;
        return filter ? value.filter((app: IPost) =>
            app.content != null && app.content.toLocaleLowerCase().indexOf(filter) != -1
            || app.shortDescription != null && app.shortDescription.toLocaleLowerCase().indexOf(filter) != -1
        ) : value;
    }

}