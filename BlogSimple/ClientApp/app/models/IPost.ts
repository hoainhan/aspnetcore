﻿export class IPost {
    id: string
    shortDescription: string
    content: string
    thumbnailImage: any
    image: any
    categoryId:any
}