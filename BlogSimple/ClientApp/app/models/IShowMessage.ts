﻿export interface IShowMessage{
  type: number,
  message: string,
  date: Date
}