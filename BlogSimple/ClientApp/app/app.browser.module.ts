import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestService } from "./services/test.service";
import { AuthGuard } from '../app/components/auth-guard.service'
import { AuthService } from '../app/components/auth.service'
import { SelectivePreloadingStrategy } from '../app/components/selective-preloading-strategy'
import { httpInterceptorProviders } from '../app/components/httpinterceptors/index'
import { ApiService } from './services/api.service'
import { CategoryService } from './services/category.service'
import { BlogPostService } from './services/blogpost.service'
import { ShareService } from './services/shareservice'
import { RouterModule } from '@angular/router';
import {AppErrorHandler } from './error-handle-message'

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        RouterModule,
      AppModuleShared,
        BrowserAnimationsModule
    ],
    providers: [
      { provide: 'BASE_URL', useFactory: getBaseUrl },
      { provide: ErrorHandler, useClass: AppErrorHandler },
      TestService,
      AuthGuard,
        AuthService,
        ApiService,
      CategoryService,
      BlogPostService,
      ShareService,
        SelectivePreloadingStrategy,
        httpInterceptorProviders
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
