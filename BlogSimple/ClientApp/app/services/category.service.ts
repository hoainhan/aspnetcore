﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { ApiService } from '../services/api.service'
import { Observable } from 'rxjs/Observable';
import { ICategory } from '../models/ICategory';

@Injectable()
export class CategoryService {
  constructor(private http: HttpClient, private api: ApiService) {

    }
    public Create(category: any): Observable<Boolean> {
       return Observable.create(ob => {
            this.api.post('category/Create', category).subscribe(result => {
               return ob.next(result);
            }, error => console.error(error));
        });
  }
    public Update(category: any): Observable<Boolean> {
      return Observable.create(ob => {
        this.api.post('category/update', category).subscribe(result => {
          return ob.next(result);
        }, error => console.error(error));
      });
    }
    public Delete(id:string): Observable<Boolean> {
      return Observable.create(ob => {
        this.api.post('category/delete', { Id: id }).subscribe(result => {
          return ob.next(result);
        }, error => console.error(error));
      });
    }
    public getlist(): Observable<ICategory[]> {
        return Observable.create(ob => {
            this.api.get("category/GetAll").subscribe(result => {
                return ob.next(result);
            });
        });
    }
    public getbyid(id: string): Observable<ICategory> {
      return Observable.create(ob => {
        this.api.post("category/getbyid", { Id: id }).subscribe(result => {
        return  ob.next(result);
        });
      });
    }


}