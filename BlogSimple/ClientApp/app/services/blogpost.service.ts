﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ApiService} from './api.service'
import { IPost } from '../models/IPost';

@Injectable()
export class BlogPostService {
    private url: string = "";
    constructor(private api: ApiService, private router: Router) {
      
    }

    public Create(post: any): Observable<Boolean> {
        return Observable.create(ob => {
            this.api.post('post/Create', post).subscribe(result => {
                return ob.next(result);
            }, error => console.error(error));
        });
    }
    public Update(category: any): Observable<Boolean> {
        return Observable.create(ob => {
            this.api.post('post/update', category).subscribe(result => {
                return ob.next(result);
            }, error => console.error(error));
        });
    }
    public Delete(id: string): Observable<Boolean> {
        return Observable.create(ob => {
            this.api.post('post/delete', { Id: id }).subscribe(result => {
                return ob.next(result);
            }, error => console.error(error));
        });
    }
    public getlist(): Observable<IPost[]> {
        return Observable.create(ob => {
          this.api.get("post/GetAll").subscribe(result => {
                return ob.next(result);
            });
        });
    }
    public getbyid(id: string): Observable<IPost> {
        return Observable.create(ob => {
            this.api.post("post/getbyid", { Id: id }).subscribe(result => {
                return ob.next(result);
            });
        });
    }

}