﻿import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Config } from "./config.service";

export interface RequestOptionsArgs {
    headers?: HttpHeaders |
    {
        [header: string]: string | string[];
       
    };
    observe?: "body";
    params?: HttpParams |
    {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType: "json";
    withCredentials?: boolean;
}

@Injectable()
export class ApiService {
    private urlPrefix: string = "";

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
        this.urlPrefix = baseUrl;
    }


    private attachPrefixUrl(url: string): string {
        return this.urlPrefix + "api/"+  url;
    }

    private attachSettings(options?: RequestOptionsArgs): RequestOptionsArgs {
        options = options == null ? { responseType: "json" } : options;
        options.withCredentials = true;
        options.headers = options.headers != null ? options.headers : new HttpHeaders();
        if (!(options.headers instanceof HttpHeaders)) {
            options.headers = new HttpHeaders(options.headers);
        }
        options.headers.append("Content-Type", "application/json; charset=utf-8");
        return options;
    }

    /**
      * Performs a request with `get` http method.
      */
    get<T>(url: string, options?: RequestOptionsArgs): Observable<T> {
        const res = this.http.get<T>(this.attachPrefixUrl(url), this.attachSettings(options));
        return res;
    }

    /**
     * Performs a request with `post` http method.
     */
    post<T>(url: string, body: any, options?: RequestOptionsArgs): Observable<T> {
        const res = this.http.post<T>(this.attachPrefixUrl(url), body, this.attachSettings(options));
        return res;
    }
}
