﻿import { Injectable } from '@angular/core'
import { Subject } from 'rxjs/Subject';
import { IShowMessage } from "../models/IShowMessage";

@Injectable()
export class ShareService {
  private showMessage = new Subject<IShowMessage>();
  showMessage$ = this.showMessage.asObservable();
  constructor() {

  }
  publishShowMessage(data: IShowMessage) {
    this.showMessage.next(data);
  }
}