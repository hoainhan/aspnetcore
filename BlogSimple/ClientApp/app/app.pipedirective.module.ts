﻿import { NgModule } from '@angular/core';
import { Format } from './directives/format';
import { OrderBy } from './directives/orderby';
import { DataGrid } from './directives/datagrid/datagrid.component';
import { PostFilterPipe } from './directives/post.pipe';
import { CategoryFilterPipe } from './directives/category.pipe'
import { CommonModule } from '@angular/common';
import { ControlMessagesComponent } from './directives/control-messages.component'
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    Format, OrderBy, DataGrid, PostFilterPipe, CategoryFilterPipe, ControlMessagesComponent
  ],
  exports: [
    Format, OrderBy, DataGrid, PostFilterPipe, CategoryFilterPipe, ControlMessagesComponent
  ]
})
export class AppPipeDirectiveModule { }