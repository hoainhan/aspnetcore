﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogSimple.Cache;
using BlogSimple.Infastructure;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Models;
using BlogSimple.Repository.UnitOfWork;
using BlogSimple.Repository.UserRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlogSimple.Pages
{
    [Authorize]
    public class UpdateUserModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
  
        public UpdateUserModel(IUserRepository userRepository, IUnitOfWork unitOfWork )
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
        
        [BindProperty]
        public UserModel userModel { get; set; }

        public void OnGet()
        {
            var userId = MyCache.GetCacheCommon != null ? MyCache.GetCacheCommon.UserId : Guid.Empty;
            userModel = _userRepository.GetAll().Where(x => x.Id == userId).FirstOrDefault().ToModel();
            //update confirm password
            userModel.CPassword = userModel.Password;
        }
        public IActionResult OnPost()
        {
            if(ModelState.IsValid)
            {
                _userRepository.Update(userModel.ToEntiy());
                _unitOfWork.Commit();
                
                ViewData["Message"] = "Update Successfully";
            }
            
            return Page();
        }
    }
}