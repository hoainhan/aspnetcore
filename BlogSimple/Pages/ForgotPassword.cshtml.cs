using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using BlogSimple.Repository.UserRepo;
using Microsoft.Extensions.Localization;
using BlogSimple.Repository.UnitOfWork;
using BlogSimple.Infastructure.Encryption;

namespace BlogSimple.Pages
{
    public class ForgotPasswordModel : PageModel
    {
        private IUserRepository _userRepository;
        private IStringLocalizer _stringLocalizer;
        private IUnitOfWork _unitOfWork;
        public ForgotPasswordModel(IUserRepository userRepository,
            IStringLocalizerFactory stringLocalizer, IUnitOfWork unitOfWork)
        {
            _stringLocalizer = stringLocalizer.Create(null);
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
        [Required]
        [BindProperty]
        public string Email { get; set; }

        [Required]
        [BindProperty]
        [Compare("RePassword", ErrorMessage = "Password and Confirm Password is not same ")]
        public string Password { get; set; }

        [Required]
        [BindProperty]
        [Compare("Password", ErrorMessage = "Password and Confirm Password is not same ")]
        public string RePassword { get; set; }

        public void OnGet()
        {
        }
        public async Task<ActionResult> OnPostAsync()
        {
            var getAllUser = await _userRepository.GetAllAsync();
            var getUser = getAllUser.Where(x => x.Email == Email)?.FirstOrDefault();
            if(getUser==null)
            {
                ModelState.AddModelError("Email", _stringLocalizer["Email is not exist"]);
                return Page();
            }
            getUser.Password = Encryption.Encrypt(Password.ToString());
            _userRepository.Update(getUser);
            _unitOfWork.Commit();
             return Redirect("/Login");
        }
    }
}