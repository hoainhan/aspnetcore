﻿using BlogSimple.Infastructure.Encryption;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Models;
using BlogSimple.Repository.UnitOfWork;
using BlogSimple.Repository.UserRepo;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSimple.Pages
{
    public class CreateUserModel : PageModel
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateUserModel(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        [BindProperty]
        public UserModel userModel { get; set; }

        public void OnGetAsync()
        {
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid)
            {
                var existUser = this._userRepository.where(x => x.UserName == userModel.UserName || x.Email == userModel.Email).ToList();
                if(existUser.Any())
                {
                    ModelState.AddModelError("CreateUser", "This user is exist");
                    return Page();
                }
                userModel.Password = Encryption.Encrypt(userModel.Password);
                var result = await _userRepository.InsertAsync(userModel.ToEntiy());
                _unitOfWork.Commit();
                return Redirect("/Login");
            }
            return Page();
        }
    }
}