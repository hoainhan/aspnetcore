﻿using BlogSimple.Cache;
using BlogSimple.Helpers;
using BlogSimple.Infastructure.Encryption;
using BlogSimple.Model.Domain;
using BlogSimple.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BlogSimple.Pages
{
    public class LoginModel : PageModel
    {
        #region Constructor
        private IUserService _userService;
        private BlogSimpleConfig _config;
        public LoginModel(IUserService userService, BlogSimpleConfig config)
        {
            _userService = userService;
            _config = config;
        }
        #endregion

        #region Public
        [Required]
        [BindProperty]
        public string UserName { get; set; }

        [Required]
        [BindProperty]
        public string Password { get; set; }

        public string MessageError { get; set; }

        public void OnGet() {}

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = _userService.UserLogin(UserName, Encryption.Encrypt(Password));
                if (user != null)
                {
                    var loadClaim = LoadClaimPrincipal(user);
                    //load claim and sign in
                    await HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: LoadClaimPrincipal(user));

                    //after login success then generate a token
                    var token = new JwtSecurityToken
                    (
                        issuer:_config.Issuer,
                        audience: _config.Audience,
                        claims: loadClaim.Claims,
                        expires: DateTime.UtcNow.AddDays(60),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey
                                    (Encoding.UTF8.GetBytes(_config.Key)),
                                SecurityAlgorithms.HmacSha256)
                    );
                    var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);
                    //init cache and cache common
                    MyCache.SetCacheCommon(new CommonCache { UserId = user.Id, Email = user.Email, UserName = user.UserName, Token = tokenValue });

                    if(Request.QueryString.Value.IndexOf("ReturnUrl") != -1 )
                    {
                        return Page();
                    }
                    return Redirect("/");
                }
                else
                {
                    ModelState.AddModelError("Login", "Password or UserName is invalid");
                    return Page();
                }
            }
            else
            {
                return Page();
            }
        }
        #endregion

        #region Private
        private ClaimsPrincipal LoadClaimPrincipal(User user)
        {
            List<Claim> claims = new List<Claim>() {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim("Admin", user.Email)
                    };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            return principal;
        }
        #endregion
    }
}