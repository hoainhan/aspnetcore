﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogSimple.Model.Domain;
using BlogSimple.Service;

namespace BlogSimple.Service
{
    public interface ICategoryService: IEntityService<Category>
    {
        Category UpdateCategory(Category category);
    }
}
