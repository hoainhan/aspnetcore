﻿
using BlogSimple.Cache;
using BlogSimple.Model.Common;
using BlogSimple.Repository;
using BlogSimple.Repository.UnitOfWork;
using System;
using System.Collections.Generic;

namespace BlogSimple.Service
{
    public class EntityService<T> : IEntityService<T> where T: BaseEntity
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<T> _repository;

        public EntityService( IGenericRepository<T> genericRepository, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = genericRepository;
        }
        public T Create(T entity)
        {
            _repository.Insert(GenerateInfoCreateUpdate(entity));
            _unitOfWork.Commit();
            return entity;
        }

        public void Delete(T entity)
        {
            _repository.Delete(entity);
            _unitOfWork.Commit();
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public T GetById(Guid Id)
        {
            return _repository.GetById(Id);
        }

        public T Update(T entity)
        {
            _repository.Update(entity);
            _unitOfWork.Commit();
            return entity;
        }

        private T GenerateInfoCreateUpdate(T entity)
        {
            var userId = MyCache.GetCacheCommon.UserId;
            if (entity.GetType().GetProperty("CreatedBy") != null )
            {
                entity.GetType().GetProperty("CreatedBy").SetValue(entity, userId);
            }
            if (entity.GetType().GetProperty("UpdatedBy") != null)
            {
                entity.GetType().GetProperty("UpdatedBy").SetValue(entity, userId);
            }
            if (entity.GetType().GetProperty("CreatedDate") != null )
            {
                entity.GetType().GetProperty("CreatedDate").SetValue(entity, DateTime.Now);
            }
            if (entity.GetType().GetProperty("UpdatedDate") != null)
            {
                entity.GetType().GetProperty("UpdatedDate").SetValue(entity, DateTime.Now);
            }
            return entity;
        }
    }
}
