﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSimple.Service
{
    public interface IEntityService<T>
    {
        T Create(T entity);
        T GetById(Guid Id);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        T Update(T entity);
    }
}
