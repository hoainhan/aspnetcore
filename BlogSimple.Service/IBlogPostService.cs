﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogSimple.Model.Domain;
using BlogSimple.Service;

namespace BlogSimple.Service
{
    public interface IBlogPostService: IEntityService<BlogPost>
    {
        BlogPost UpdateCustom(BlogPost blogPost);
    }
}
