﻿using BlogSimple.Cache;
using BlogSimple.Model.Domain;
using BlogSimple.Repository;
using BlogSimple.Repository.UnitOfWork;
using System;
using System.Linq;

namespace BlogSimple.Service
{
    public class CategoryService : EntityService<Category>, ICategoryService
    {
        #region Constructor
        private ICategoryRepository _categoryRepository;
        private IUnitOfWork _unitOfWork;
        public CategoryService(ICategoryRepository categoryRepository, IUnitOfWork unitOfWork) : base(categoryRepository, unitOfWork)
        {
            _categoryRepository = categoryRepository;
            _unitOfWork = unitOfWork;
        }


        #endregion

        #region Methods

        public Category UpdateCategory(Category category)
        {
            try
            {
                var getCategory = this.GetById(category.Id);
                getCategory.Description = category.Description;
                getCategory.Name = category.Name;
                getCategory.UpdatedBy = MyCache.GetCacheCommon.UserId;
                getCategory.UpdatedDate = DateTime.Now;
                return this.Update(getCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
