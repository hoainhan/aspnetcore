﻿using BlogSimple.Model.Domain;
using BlogSimple.Repository.UnitOfWork;
using BlogSimple.Repository.UserRepo;
using System.Linq;
using System;

namespace BlogSimple.Service
{
    public class UserService : EntityService<User>, IUserService
    {
        private IUserRepository _userRepository;
        private IUnitOfWork _unitOfWork;
        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork) : base(userRepository, unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public string GetName(Guid userId)
        {
            var user = this.GetById(userId);
            return user.FirstName + " " + user.LastName;
        }

        public User UserLogin(string userName, string password)
        {
            try
            {
                return this._userRepository.where(x =>(x.UserName == userName || x.Email ==userName ) && x.Password == password).FirstOrDefault();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
