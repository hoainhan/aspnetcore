﻿using BlogSimple.Model.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSimple.Service
{
    public interface IUserService : IEntityService<User>
    {
        User UserLogin(string userName, string password);
        string GetName(Guid userId);
    }
}
