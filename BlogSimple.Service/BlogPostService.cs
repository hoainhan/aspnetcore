﻿using BlogSimple.Cache;
using BlogSimple.Model.Domain;
using BlogSimple.Repository;
using BlogSimple.Repository.UnitOfWork;
using System;
using System.IO;

namespace BlogSimple.Service
{
    public class BlogPostService: EntityService<BlogPost>, IBlogPostService
    {
        private IBlogPostRepository _blogPostRepository;
        private IUnitOfWork _unitOfWork;
        public BlogPostService(IBlogPostRepository blogPostRepository, IUnitOfWork unitOfWork) : base(blogPostRepository, unitOfWork)
        {
            _blogPostRepository = blogPostRepository;
            _unitOfWork = unitOfWork;
        }

        public BlogPost UpdateCustom(BlogPost blogPost)
        {
            var getPost = this._blogPostRepository.GetById(blogPost.Id);
            // clear Id for update -- issue duplicate id
            blogPost.Id = Guid.NewGuid();

            if (blogPost.Image != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    blogPost.Image.CopyTo(memoryStream);
                    getPost.ThumbnailImage = memoryStream.ToArray();
                }
            }
            getPost.ShortDescription = blogPost.ShortDescription;
            getPost.Content = blogPost.Content;
            getPost.UpdatedBy = MyCache.GetCacheCommon.UserId;
            getPost.UpdatedDate = DateTime.Now;
            return this.Update(getPost);
        }
    }
}
