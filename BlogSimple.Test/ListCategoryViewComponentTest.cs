﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Xunit;
using BlogSimple.Repository;
using BlogSimple.Model;
using AutoMapper;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Test.Helpers;
using BlogSimple.Model.Domain;
using System.Threading.Tasks;
using System.Linq;
using BlogSimple.ViewComponents;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using BlogSimple.Models;

namespace BlogSimple.Test
{
    public class ListCategoryViewComponentTest
    {
        private Mock<ICategoryRepository> _categoryRepository;
        private BlogSimpleContext context;
        public ListCategoryViewComponentTest()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ConfigMapper>();
            });

            context = new BlogSimpleContext(TestDbContextHelper.TestDbContextOptions());
            Initial();
        }
        [Fact]
        public async Task InvokeAsync_Test()
        {
            _categoryRepository = new Mock<ICategoryRepository>();
            var listCategory = context.Categories.ToList();
           
            _categoryRepository.Setup(x => x.GetAllAsync()).Returns(Task.FromResult(listCategory));
            var listPost = new ListCategoryViewComponent(_categoryRepository.Object);
            var view = await listPost.InvokeAsync() as ViewViewComponentResult;
            var model = view.ViewData.Model as List<CategoryModel>;
            Assert.Single(model);
        }
        private void Initial()
        {
            var categoryId = Guid.NewGuid();
            context.Categories.Add(new Category { Description = "categoryA", Name = "categoryA", Id = categoryId });
            context.BlogPosts.Add(new BlogPost { CategoryId = categoryId, Comments = null, Content = "this is blogA", Id = Guid.NewGuid() });
            context.SaveChanges();
        }
    }
}
