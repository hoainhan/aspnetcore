using AutoMapper;
using BlogSimple.Controllers;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Model;
using BlogSimple.Model.Domain;
using BlogSimple.Repository;
using BlogSimple.Service;
using BlogSimple.Test.Helpers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace BlogSimple.Test
{
    public class PostControllerTest
    {
        private Mock<IBlogPostRepository> _blogPostRepository;
        private Mock<IBlogPostService> _blogPostService;
        private BlogSimpleContext context;
        public PostControllerTest() {

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ConfigMapper>();
            });

            context = new BlogSimpleContext(TestDbContextHelper.TestDbContextOptions());
            Initial();
        }
        
        [Fact]
        public void A1_Post_Index_Test()
        {
            _blogPostService = new Mock<IBlogPostService>();
            _blogPostRepository = new Mock<IBlogPostRepository>();
             var postController = new PostController(_blogPostRepository.Object,_blogPostService.Object);
             var index = postController.Index() as ViewResult;
             Assert.True(index.ViewData !=null);
        }
        [Fact]
        public void A2_Post_Get_Detail_Test() {
            
            _blogPostService = new Mock<IBlogPostService>();
            _blogPostRepository = new Mock<IBlogPostRepository>();
            var blog = context.BlogPosts.FirstOrDefault();
            _blogPostRepository.Setup(x => x.GetById(blog.Id)).Returns(blog);
            var postController = new PostController(_blogPostRepository.Object, _blogPostService.Object);
            var getDetail = postController.GetDetail(blog.Id) as ViewResult;
            Assert.True(getDetail.Model !=null);
        }
        [Fact]
        public void A3_Search_Post_Test()
        {
            _blogPostService = new Mock<IBlogPostService>();
            _blogPostRepository = new Mock<IBlogPostRepository>();
            var postController = new PostController(_blogPostRepository.Object, _blogPostService.Object);
            var search = postController.Search("test",Guid.NewGuid());
            Assert.NotNull(search);
        }
        private void Initial()
        {
          var categoryId = Guid.NewGuid();
          context.Categories.Add(new Category { Description = "categoryA", Name = "categoryA", Id = categoryId });
          context.BlogPosts.Add(new BlogPost { CategoryId=categoryId, Comments = null, Content = "this is blogA", Id = Guid.NewGuid() });
          context.SaveChanges();
        }
    }
}
