﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using BlogSimple.Repository;
using BlogSimple.Model;
using AutoMapper;
using BlogSimple.Test.Helpers;
using BlogSimple.Infastructure.Mapper;
using BlogSimple.Model.Domain;
using BlogSimple.ViewComponents;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Xunit;
using BlogSimple.Models;

namespace BlogSimple.Test
{
    public class ListPostViewComponentTest
    {
        private Mock<IBlogPostRepository> _blogPostRepository;
        private BlogSimpleContext context;
        public ListPostViewComponentTest()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ConfigMapper>();
            });

            context = new BlogSimpleContext(TestDbContextHelper.TestDbContextOptions());
            Initial();
        }
        [Fact]
        public async Task  InvokeAsync_Test() {
            _blogPostRepository = new Mock<IBlogPostRepository>();
            var category = context.Categories.FirstOrDefault();
            var post = context.BlogPosts.ToList();
            _blogPostRepository.Setup(x => x.GetAllAsync()).Returns(Task.FromResult(post));
            var listPost = new ListPostViewComponent(_blogPostRepository.Object);
            var view = await listPost.InvokeAsync("this", category.Id) as ViewViewComponentResult;
            var model = view.ViewData.Model as List<ListPostModel>;
            Assert.Single(model);
        }
        private void Initial()
        {
            var categoryId = Guid.NewGuid();
            context.Categories.Add(new Category { Description = "categoryA", Name = "categoryA", Id = categoryId });
            context.BlogPosts.Add(new BlogPost { CategoryId = categoryId, Comments = null, Content = "this is blogA", Id = Guid.NewGuid() });
            context.SaveChanges();
        }
    }
}
