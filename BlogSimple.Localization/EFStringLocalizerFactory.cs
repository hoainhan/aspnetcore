﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Localization;
using BlogSimple.Repository.Localization;

namespace BlogSimple.Localization
{
    public class EfStringLocalizerFactory : IStringLocalizerFactory
    {
        private readonly IResourceRepository _resourceRepository;
        private IList<ResourceString> _resourceStrings;

        public EfStringLocalizerFactory(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
            LoadResources();
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            return new EfStringLocalizer(_resourceStrings);
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            return new EfStringLocalizer(_resourceStrings);
        }

        private void LoadResources()
        {
            _resourceStrings = _resourceRepository.GetAll().Select(x => new ResourceString
            {
                Culture = x.CultureId,
                Key = x.Key,
                Value = x.Value
            }).ToList();
        }
    }
}