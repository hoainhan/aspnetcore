﻿using System;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;

namespace BlogSimple.Cache
{
    public class MyCache
    {
        private const string keyCommonCache = "commonCache";
        private static CancellationTokenSource _resetCache = new CancellationTokenSource();
        public static IMemoryCache _memoryCache;
        private MyCache() { }

        public static void Set<T>(T value, string key)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions();
            // Pin to cache.
            cacheEntryOptions.SetPriority(CacheItemPriority.NeverRemove);
            cacheEntryOptions.AbsoluteExpiration = DateTime.Now.AddDays(20); ;
            
            _memoryCache.Set(key, value,cacheEntryOptions);
        }

        public static T Get<T>(string key)
        {
            return _memoryCache.Get<T>(key);
        }

        public static void SetCacheCommon(CommonCache commonCache)
        {
            Set(commonCache, keyCommonCache);
        }
        public static CommonCache GetCacheCommon
        {
            get
            {
                return Get<CommonCache>(keyCommonCache);
            }
        }
        public static void Reset()
        {
            if (_resetCache != null && !_resetCache.IsCancellationRequested && _resetCache.Token.CanBeCanceled)
            {
                _resetCache.Cancel();
                _resetCache.Dispose();
            }

            _resetCache = new CancellationTokenSource();
        }
    }
    public class CommonCache
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
